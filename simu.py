from tkinter import *
from argparse import *
import random
import datetime

master = Tk()

canvas = Canvas(master,width=290, height=320)
canvas.pack()


#ligne 1
r1= canvas.create_rectangle(0,0,100,100)
r2= canvas.create_rectangle(100,0,200,100)
r3= canvas.create_rectangle(200,0,300,100)
#ligne 2
r4= canvas.create_rectangle(0,200,100,100)
r5= canvas.create_rectangle(100,200,200,100)
r6= canvas.create_rectangle(200,200,300,100)
#ligne 3
r7= canvas.create_rectangle(0,300,100,100)
r8= canvas.create_rectangle(100,300,200,100)
r9= canvas.create_rectangle(200,300,300,100)


def create_forest():
    for i in range(10):
        j = random.randint(0,1)
        print(j)
        if(j==1):
            if(i==r1):
                canvas.itemconfig(r1,fill="green")
            elif(i==r2):
                canvas.itemconfig(r1,fill="green")
            elif(i==r3):
                canvas.itemconfig(r2,fill="green")
            elif(i==r4):
                canvas.itemconfig(r3,fill="green")
            elif(i==r5):
                canvas.itemconfig(r4,fill="green")
            elif(i==r6):
                canvas.itemconfig(r5,fill="green")
            elif(i==r7):
                canvas.itemconfig(r6,fill="green")
            elif(i==r8):
                canvas.itemconfig(r7,fill="green")
            elif(i==r9):
                canvas.itemconfig(r8,fill="green")

def click_callback(event):
    x1 = event.x
    y1 = event.y
    print(x1,y1)
    color="red"
    #ligne 1
    if (x1 <= 100 and x1 >= 0 and y1 <= 100 and y1 >= 0):
        canvas.itemconfig(r1,fill=color)
    elif (x1 <= 200 and x1 >= 100 and y1 <= 100 and y1 >= 0):
        canvas.itemconfig(r2,fill=color)
    elif (x1 <= 300 and x1 >= 200 and y1 <= 100 and y1 >= 0):
        canvas.itemconfig(r3,fill=color)
    #ligne 2
    elif (x1 <= 100 and x1 >= 0 and y1 <= 200 and y1 >= 100):
        canvas.itemconfig(r4,fill=color)
    elif (x1 <= 200 and x1 >= 100 and y1 <= 200 and y1 >= 100):
        canvas.itemconfig(r5,fill=color)
    elif (x1 <= 300 and x1 >= 200 and y1 <= 200 and y1 >= 100):
        canvas.itemconfig(r6,fill=color)
    #ligne 3
    elif (x1 <= 100 and x1 >= 0 and y1 <= 300 and y1 >= 200):
        canvas.itemconfig(r7,fill=color)
    elif (x1 <= 200 and x1 >= 100 and y1 <= 300 and y1 >= 200):
        canvas.itemconfig(r8,fill=color)
    elif (x1 <= 300 and x1 >= 200 and y1 <= 300 and y1 >= 200):
        canvas.itemconfig(r9,fill=color)
    

Bouton = Button(master, text = "Simulation", anchor = W)
Bouton.pack()
canvas.bind('<Button-1>', click_callback)
create_forest()
master.mainloop()